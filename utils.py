import yaml
import psycopg2
import json
from datetime import datetime

database = 'weather_psql'


def read_config(database):
    with open("config.yml", 'r') as stream:
        try:
            return(yaml.safe_load(stream)[database])
        except yaml.YAMLError as exc:
            print(exc)


def return_nearest_station_data(lng, lat, timestamp):
    conn = None
    try:
        params = read_config(database)
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # find nearest station to given longitude/latitude
        cur.execute(sql_nearest_distance, {'longitude': lng, 'latitude': lat})
        row = cur.fetchone()
        # retrieve all readings from nearest station in given time
        station_id = row[0]
        cur.execute(
            sql_endpoint_query,
            {'station_id': station_id, 'recorded_at': datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S')}
        )
        rows = cur.fetchall()
        cur.close()
        conn.commit()
        json_data = json.dumps({'station_id': station_id, 'readings': rows})
        return json_data
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        print('closing connection')
        if conn is not None:
            conn.close()


sql_nearest_distance = """
SELECT station_id, (point(longitude, latitude) <@> point(%(longitude)s, %(latitude)s)) * 1609.344 AS distance
FROM
  station
ORDER BY
 distance
LIMIT 1;
"""
sql_endpoint_query = """
SELECT to_char(recorded_at, 'DD Mon YYYY HH:MI:SSPM') as recorded_at, pm2_5
FROM 
  reading
WHERE 
  recorded_at = %(recorded_at)s AND station_id = %(station_id)s
ORDER BY
  recorded_at
"""
