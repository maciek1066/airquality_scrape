#!/usr/bin/python
import psycopg2
from utils import read_config, database


def create_tables():
    """ create tables in the PostgreSQL database"""
    commands = (
        """CREATE EXTENSION earthdistance CASCADE""",
        """ CREATE TABLE station (
                station_id VARCHAR(64) PRIMARY KEY,
                station_name VARCHAR(128) NOT NULL,
                latitude FLOAT8,
                longitude FLOAT8
                )
        """,
        """
        CREATE TABLE reading (
                reading_id SERIAL PRIMARY KEY,
                station_id VARCHAR(64) NOT NULL,
                recorded_at TIMESTAMP NOT NULL,
                pm2_5 float NOT NULL,
                FOREIGN KEY (station_id) 
                    REFERENCES station (station_id)
                    ON UPDATE CASCADE ON DELETE CASCADE
        )
        """,
        """
        CREATE UNIQUE INDEX unique_samples ON reading (station_id, recorded_at);
        """,
        """
        CREATE INDEX station_location_idx ON station USING gist (ll_to_earth(latitude, longitude));
        """
    )
    conn = None
    try:
        # read the connection parameters
        params = read_config(database)
        # connect to the PostgreSQL server
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        # close communication with the PostgreSQL database server
        cur.close()
        # commit the changes
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    create_tables()
