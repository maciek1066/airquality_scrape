from datetime import datetime
from json.decoder import JSONDecodeError

from utils import database, read_config
import psycopg2
import requests
import json


# main page with list of all stations
url = 'https://airquality.ie/stations'

# asset xml with details about each station
url_php_assets = 'https://airquality.ie/assets/php/get-monitors.php'

headers = {
    'Accept': 'application/xml',
    'Referer': 'https://airquality.ie/stations',
}


def scrape_air_quality():
    try:
        session = requests.Session()
        session.get(url)
        response = session.get(url=url_php_assets, headers=headers)
        stations = json.loads(response.content)
    except JSONDecodeError as exc:
        print('Error')
        raise exc
    # records to insert
    station_records = []
    reading_records = []
    for station in stations:
        # station specific data
        try:
            station_data = {
                'station_id': station['serial_number'],
                'station_name': '{}. {}'.format(station['label'], station['location']),
                'latitude': station['latitude'],
                'longitude': station['longitude'],
            }
            # measurement data
            reading_data = {
                'station_id': station['serial_number'],
                'pm2_5': station['latest_reading']['pm2_5'],
                'recorded_at': datetime.strptime(station['latest_reading']['recorded_at'], '%Y-%m-%d %H:%M:%S')
            }
        except (TypeError, KeyError):
            continue
        station_records.append(station_data)
        if reading_data['pm2_5']:
            reading_records.append(reading_data)
    return station_records, reading_records


def upsert_weather_data(stations, readings):
    conn = None
    try:
        params = read_config(database)
        conn = psycopg2.connect(**params)
        cur = conn.cursor()
        cur.executemany(
            """INSERT INTO station (station_id, station_name, latitude, longitude)
            VALUES (%(station_id)s, %(station_name)s, %(latitude)s, %(longitude)s)
            """,
            stations
        )
        cur.executemany(
            """INSERT INTO reading (station_id, pm2_5, recorded_at) 
            VALUES (%(station_id)s, %(pm2_5)s, %(recorded_at)s)
            """,
            readings
        )
        cur.close()
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        print('closing connection')
        if conn is not None:
            conn.close()


if __name__ == '__main__':
    stations, readings = scrape_air_quality()
    upsert_weather_data(stations, readings)
    print('finished')
