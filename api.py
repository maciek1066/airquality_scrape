from utils import return_nearest_station_data
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

app = Flask(__name__)
api = Api(app)


def abort_if_doesnt_exist(data):
    if not data:
        abort(404, message="Results for given parameters does not exist")


parser = reqparse.RequestParser()


class NearestStationReadingsAPI(Resource):
    def get(self, lng, lat, timestring):
        json_data = return_nearest_station_data(lng, lat, timestring)
        abort_if_doesnt_exist(json_data)
        return json_data


api.add_resource(NearestStationReadingsAPI, '/readings/<lng>&<lat>&<timestring>')

if __name__ == '__main__':
    app.run(debug=True)